import RPi.GPIO as GPIO 
import time

def init(pin):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin,GPIO.OUT)
    GPIO.setwarnings(False)

def blinkslow(pin):
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(pin, GPIO.LOW)
    time.sleep(0.5)

def blinkfast(pin):
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(0.15)
    GPIO.output(pin, GPIO.LOW)
    time.sleep(0.15)

PIN = 4
init(PIN)
for i in range(0,2):
    # S
    for i in range(0,3):
        blinkfast(PIN)
    time.sleep(0.35)
    # O
    for i in range(0,3):
        blinkslow(PIN)
    # S
    for i in range(0,3):
        blinkfast(PIN)
    # Pause before starting over
    time.sleep(1.25)

GPIO.cleanup()
