import RPi.GPIO as GPIO 
import time

def init(pin):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin,GPIO.OUT)
    GPIO.setwarnings(False)

def blink(pin):
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(1)
    GPIO.output(pin, GPIO.LOW)
    time.sleep(1)

def blinkfast(pin):
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(0.25)
    GPIO.output(pin, GPIO.LOW)
    time.sleep(0.25)

PIN = 4

init(PIN)
for i in range(0,5):
    blink(PIN)

for i in range(0,10):
    blinkfast(PIN)

GPIO.cleanup()
