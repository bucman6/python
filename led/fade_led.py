import RPi.GPIO as GPIO 
import time
import math

def fadeLED(percent):
	p.ChangeDutyCycle(percent)

LIGHT_PIN = 4
FREQUENCY_IN_HERTZ = 50
SECONDS_TO_SLEEP = 0.025
DUTY_CYCLE = 0.0  # Time between pulses

GPIO.setmode(GPIO.BCM) # Use Board Numbering
GPIO.setup(LIGHT_PIN, GPIO.OUT) # Setup light for output
p = GPIO.PWM(LIGHT_PIN, FREQUENCY_IN_HERTZ) # Configure 

p.start(DUTY_CYCLE)

i = 0
while i < 100.0:
	fadeLED(i)
	time.sleep(SECONDS_TO_SLEEP)	
	i += 1

i = 0
while i < 100.0:
	fadeLED(100.0 - i)
	time.sleep(SECONDS_TO_SLEEP)
	i += 1

p.stop()
GPIO.cleanup()
