import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

def RCtime(RCpin):
	reading = 0
	GPIO.setup(RCpin, GPIO.OUT)
	GPIO.output(RCpin, GPIO.LOW)
	time.sleep(0.1)

	GPIO.setup(RCpin, GPIO.IN)
	while(GPIO.input(RCpin) == GPIO.LOW):
		reading += 1

	return reading

i = 0
while(i<25):
	print RCtime(25)
	i += 1
	time.sleep(0.1)

