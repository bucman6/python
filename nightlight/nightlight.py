import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

def init(lightSensorPin, ledPin):
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(lightSensorPin, GPIO.OUT)
	GPIO.setup(ledPin, GPIO.OUT)
	return

def initLED():
	GPIO.setup(pin, GPIO.OUT)

def getLightValue(pin):
	reading = 0
	GPIO.setup(pin, GPIO.OUT)
	GPIO.output(pin, GPIO.LOW)
	time.sleep(0.1)

	GPIO.setup(pin, GPIO.IN)
	while(GPIO.input(pin) == GPIO.LOW):
		reading += 1

	return reading

def tripLight(pin, state):
	GPIO.output(pin, state)
	return

def lightIsOff(lightPin):
	return (GPIO.input(lightPin) == GPIO.LOW)

i = 0
init(25, 18)
while(i<100):
	lightValue = getLightValue(25)
	if(lightValue > 1500):
		tripLight(18, GPIO.HIGH)
	elif(lightValue <= 1500):
		tripLight(18, GPIO.LOW)
	
	print "Light Value := " + str(lightValue)
	i += 1
	time.sleep(0.1)

GPIO.cleanup()
