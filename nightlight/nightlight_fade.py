import RPi.GPIO as GPIO 
import time
import math

def fadeLED(percent):
	p.ChangeDutyCycle(percent)

def getCapacitorTiming(pin):
	GPIO.setup(pin, GPIO.OUT)
	GPIO.output(pin, GPIO.LOW)
	time.sleep(SECONDS_TO_SLEEP)
	
	GPIO.setup(pin, GPIO.IN)
	timer = 0
	while GPIO.input(pin) == GPIO.LOW:
		timer += 1

	return timer

def convertCapacitorTimingToPercent(capacitorTiming):
	percent = math.floor((capacitorTiming/MAX_CAPACITOR_VALUE) * 100)
	if percent > MAX_PERCENT:
		percent = MAX_PERCENT
	elif percent < MIN_PERCENT:
		percent = 0

	return percent

#######################################################################3

LIGHT_PIN = 18
SENSOR_PIN = 25
FREQUENCY_IN_HERTZ = 50
SECONDS_TO_SLEEP = 0.1
DUTY_CYCLE = 0.0  # Time between pulses
MAX_CAPACITOR_VALUE = 8015.0
MAX_PERCENT = 100.0
MIN_PERCENT = 15.0

GPIO.setmode(GPIO.BCM) # Use Board Numbering
GPIO.setup(LIGHT_PIN, GPIO.OUT) # Setup light for output
p = GPIO.PWM(LIGHT_PIN, FREQUENCY_IN_HERTZ) # Configure 

p.start(DUTY_CYCLE)

count = 0
while count < 200.0:
	capacitorTiming = getCapacitorTiming(SENSOR_PIN)
	percent = convertCapacitorTimingToPercent(capacitorTiming)
	fadeLED(percent)	
	count += 1
	print "Capacitor := " + str(capacitorTiming) + "	Percent := " + str(percent) + "%"

p.stop()
GPIO.cleanup()
