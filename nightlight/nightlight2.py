import RPi.GPIO as GPIO
import time
import math

def init(lightSensorPin, ledPin):
	GPIO.setup(lightSensorPin, GPIO.OUT)
	GPIO.setup(ledPin, GPIO.OUT)
	return

def initLED():
	GPIO.setup(pin, GPIO.OUT)

def getCapacitorTiming(pin):
	GPIO.setup(pin, GPIO.OUT)
	GPIO.output(pin, GPIO.LOW)
	time.sleep(SECONDS_TO_SLEEP)

	GPIO.setup(pin, GPIO.IN)
	
	reading = 0
	while(GPIO.input(pin) == GPIO.LOW):
		reading += 1

	return reading

def fadeLight(pin, percent):
	p.ChangeDutyCycle(percent)

def getLightPercent(sensorReading):
	percent = math.floor((sensorReading/MAX_CAPACITOR_TIMING) * 100)
	if percent > MAX_PERCENT:
		percent = MAX_PERCENT 
	elif percent < MIN_PERCENT:
		percent = 0 

	return percent

GPIO.setmode(GPIO.BCM)

LIGHT_PIN = 18
SENSOR_PIN = 25
MAX_ITTERATIONS = 100
MIN_PERCENT = 15
MAX_PERCENT = 100
HERTZ = 100
SECONDS_TO_SLEEP = 0.1
MAX_CAPACITOR_TIMING = 8015.0 
#5015.0

GPIO.setup(LIGHT_PIN, GPIO.OUT)
p = GPIO.PWM(LIGHT_PIN, HERTZ)
p.start(0)

init(SENSOR_PIN, LIGHT_PIN)

count = 0
while(count < MAX_ITTERATIONS):
	lightValue = getCapacitorTiming(SENSOR_PIN)

	percent = getLightPercent(lightValue)	
	print "Percent := " + str(percent)
	print "Light Value := " + str(lightValue)

	fadeLight(LIGHT_PIN, percent)
	count += 1
	time.sleep(SECONDS_TO_SLEEP)

p.stop()
GPIO.cleanup()
